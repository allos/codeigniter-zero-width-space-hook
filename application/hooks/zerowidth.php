<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Codeigniter Zero Width Space Hook
 *
 *	This hooks remove zero width space character from content. 
 *	Common issue is the "&#8203;" character on the page source 
 *	and the "%E2%80%8B" character on copy-paste links.
 *
 * @package     zerowidth
 * @author      Nikos Kornarakis
 * @copyright   Copyright (c) Nikos Kornarakis
 * @license     http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @link        http://nikos.kornarakis.gr
 * @since       Version 1.0
 */  
function zerowidth()
{
	$CI =& get_instance();
	$buffer = $CI->output->get_output();
	$buffer = preg_replace("/\xE2\x80\x8B/", "", $buffer); 
	$CI->output->set_output($buffer);
	$CI->output->_display();
}
 
/* End of file zerowidth.php */
/* Location: ./system/application/hooks/zerowidth.php */